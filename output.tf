output "instance_ip_addr" {
  value = resource.digitalocean_droplet.devops_terraform_task3.ipv4_address
}
