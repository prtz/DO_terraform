data "gitlab_user" "rebrain_user" {
  username = var.gitlab_username
}

data "gitlab_group" "rebraim_user_group" {
  full_path = "devops_users_repos/${data.gitlab_user.rebrain_user.id}"
}

resource "gitlab_project" "rebrain_devops_terraform_task3" {
  name             = "rebrain-devops-terraform-task3"
  namespace_id     = data.gitlab_group.rebraim_user_group.id
  description      = "Terraform_task_3"
  visibility_level = "private"
}

# resource "gitlab_deploy_key" "rebrain_devops_key1" {
#   project  = resource.gitlab_project.rebrain_devops_terraform_task3.path_with_namespace
#   title    = "rebrain_devops_key1"
#   key      = var.key
#   can_push = true
# }
