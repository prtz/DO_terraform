#######################################################
#### Variables to configure (or be prompted about) ####
#######################################################

variable "gitlab_token" {
  description = "your gitlab token"
  sensitive   = true
}

variable "gitlab_username" {
  description = "your rebrain gitlab username"
  sensitive   = true
}

variable "key" {
  description = "your ssh public key that will add to GitLab project"
  sensitive   = true
}

variable "do_token" {
  description = "your DO token"
  sensitive   = true
}

variable "my_pub_key" {
  description = "your pub SSH key for add to DO"
  sensitive   = true
}

variable "rebraine_ssh_pub_key" {
  description = "your pub SSH key for add to DO"
  sensitive   = true
  type        = string
}
#######################################################
######### Variables you may want to configure #########
#######################################################

variable "gitlab_base_url" {
  description = "gitlab API endpoint"
  default     = "https://gitlab.rebrainme.com/api/v4/"
}
