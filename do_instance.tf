data "digitalocean_ssh_key" "USER_SSH_PUB_KEY" {
  name = "USER.SSH.PUB.KEY"
}
# API request
data "http" "ssh_search" {
  url = "https://api.digitalocean.com/v2/account/keys?per_page=200"

  # Optional request headers
  request_headers = {
    Content-Type  = "application/json"
    Authorization = "Bearer ${var.do_token}"

  }
}
# Search key ID thrue pub key 
data "jq_query" "REBRAIN_SSH_PUB_KEY" {
  data  = data.http.ssh_search.response_body
  query = ".ssh_keys [] | select(.public_key == ${file("rebrain_key.pub")}) | .id"
}

resource "digitalocean_tag" "terraform_03" {
  name = "terraform-03"
}
resource "digitalocean_tag" "devops" {
  name = "devops"
}
resource "digitalocean_tag" "user_email" {
  name = var.gitlab_username
}

resource "digitalocean_droplet" "devops_terraform_task3" {
  image    = "ubuntu-22-04-x64"
  name     = "devops-terraform-task2"
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  ssh_keys = [data.jq_query.REBRAIN_SSH_PUB_KEY.result, data.digitalocean_ssh_key.USER_SSH_PUB_KEY.id]
  tags     = [resource.digitalocean_tag.terraform_03.id, resource.digitalocean_tag.devops.id, resource.digitalocean_tag.user_email.id]
}
