terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.18.0"
    }
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.22.3"
    }
    http = {
      source  = "hashicorp/http"
      version = "3.1.0"
    }
    jq = {
      source  = "massdriver-cloud/jq"
      version = "0.2.0"
    }
  }
}

provider "gitlab" {
  token    = var.gitlab_token
  base_url = var.gitlab_base_url
}

provider "digitalocean" {
  token = var.do_token
}

provider "http" {
  # Configuration options
}

provider "jq" {
  # Configuration options
}
